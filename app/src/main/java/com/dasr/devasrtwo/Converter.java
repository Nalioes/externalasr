package com.dasr.devasrtwo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Converter {
	
    public static String uni512zg1(String input) {

        String rule = "[ { \"from\": \"\u1004\u103a\u1039\", \"to\": \"\u1064\" }, { \"from\": \"\u1039\u1010\u103d\", \"to\": \"\u1096\" }, { \"from\": \"\u1014(?=[\u1030\u103d\u103e\u102f\u1039])\", \"to\": \"\u108f\" }, { \"from\": \"\u102b\u103a\", \"to\": \"\u105a\" }, { \"from\": \"\u100b\u1039\u100c\", \"to\": \"\u1092\" }, { \"from\": \"\u102d\u1036\", \"to\": \"\u108e\" }, { \"from\": \"\u104e\u1004\u103a\u1038\", \"to\": \"\u104e\" }, { \"from\": \"[\u1025\u1009](?=[\u1039\u102f\u1030])\", \"to\": \"\u106a\" }, { \"from\": \"[\u1025\u1009](?=[\u103a])\", \"to\": \"\u1025\" }, { \"from\": \"\u100a(?=[\u1039\u102f\u1030\u103d])\", \"to\": \"\u106b\" }, { \"from\": \"(\u1039[\u1000-\u1021])\u102f\", \"to\": \"$1\u1033\" }, { \"from\": \"(\u1039[\u1000-\u1021])\u1030\", \"to\": \"$1\u1034\" }, { \"from\": \"\u1039\u1000\", \"to\": \"\u1060\" }, { \"from\": \"\u1039\u1001\", \"to\": \"\u1061\" }, { \"from\": \"\u1039\u1002\", \"to\": \"\u1062\" }, { \"from\": \"\u1039\u1003\", \"to\": \"\u1063\" }, { \"from\": \"\u1039\u1005\", \"to\": \"\u1065\" }, { \"from\": \"\u1039\u1007\", \"to\": \"\u1068\" }, { \"from\": \"\u1039\u1008\", \"to\": \"\u1069\" }, { \"from\": \"\u100a(?=[\u1039\u102f\u1030])\", \"to\": \"\u106b\" }, { \"from\": \"\u1039\u100b\", \"to\": \"\u106c\" }, { \"from\": \"\u1039\u100c\", \"to\": \"\u106d\" }, { \"from\": \"\u100d\u1039\u100d\", \"to\": \"\u106e\" }, { \"from\": \"\u100e\u1039\u100d\", \"to\": \"\u106f\" }, { \"from\": \"\u1039\u100f\", \"to\": \"\u1070\" }, { \"from\": \"\u1039\u1010\", \"to\": \"\u1071\" }, { \"from\": \"\u1039\u1011\", \"to\": \"\u1073\" }, { \"from\": \"\u1039\u1012\", \"to\": \"\u1075\" }, { \"from\": \"\u1039\u1013\", \"to\": \"\u1076\" }, { \"from\": \"\u1039\u1013\", \"to\": \"\u1076\" }, { \"from\": \"\u1039\u1014\", \"to\": \"\u1077\" }, { \"from\": \"\u1039\u1015\", \"to\": \"\u1078\" }, { \"from\": \"\u1039\u1016\", \"to\": \"\u1079\" }, { \"from\": \"\u1039\u1017\", \"to\": \"\u107a\" }, { \"from\": \"\u1039\u1018\", \"to\": \"\u107b\" }, { \"from\": \"\u1039\u1019\", \"to\": \"\u107c\" }, { \"from\": \"\u1039\u101c\", \"to\": \"\u1085\" }, { \"from\": \"\u103f\", \"to\": \"\u1086\" }, { \"from\": \"(\u103c)\u103e\", \"to\": \"$1\u1087\" }, { \"from\": \"\u103d\u103e\", \"to\": \"\u108a\" }, { \"from\": \"(\u1064)([\u1031]?)([\u103c]?)([\u1000-\u1021])\u102d\", \"to\": \"$2$3$4\u108b\" }, { \"from\": \"(\u1064)([\u1031]?)([\u103c]?)([\u1000-\u1021])\u102e\", \"to\": \"$2$3$4\u108c\" }, { \"from\": \"(\u1064)([\u1031]?)([\u103c]?)([\u1000-\u1021])\u1036\", \"to\": \"$2$3$4\u108d\" }, { \"from\": \"(\u1064)([\u1031]?)([\u103c]?)([\u1000-\u1021])\", \"to\": \"$2$3$4$1\" }, { \"from\": \"\u101b(?=[\u102f\u1030\u103d\u108a])\", \"to\": \"\u1090\" }, { \"from\": \"\u100f\u1039\u100d\", \"to\": \"\u1091\" }, { \"from\": \"\u100b\u1039\u100b\", \"to\": \"\u1097\" }, { \"from\": \"([\u1000-\u1021\u1029\u1090])([\u1060-\u1069\u106c\u106d\u1070-\u107c\u1085\u108a])?([\u103b-\u103e]*)?\u1031\", \"to\": \"\u1031$1$2$3\" }, { \"from\": \"([\u1000-\u1021\u1029])([\u1060-\u1069\u106c\u106d\u1070-\u107c\u1085])?(\u103c)\", \"to\": \"$3$1$2\" }, { \"from\": \"\u103a\", \"to\": \"\u1039\" }, { \"from\": \"\u103b\", \"to\": \"\u103a\" }, { \"from\": \"\u103c\", \"to\": \"\u103b\" }, { \"from\": \"\u103d\", \"to\": \"\u103c\" }, { \"from\": \"\u103e\", \"to\": \"\u103d\" }, { \"from\": \"\u103d\u102f\", \"to\": \"\u1088\" }, { \"from\": \"([\u102f\u1030\u1014\u101b\u103c\u108a\u103d\u1088])([\u1032\u1036]{0,1})\u1037\", \"to\": \"$1$2\u1095\" }, { \"from\": \"\u102f\u1095\", \"to\": \"\u102f\u1094\" }, { \"from\": \"([\u1014\u101b])([\u1032\u1036\u102d\u102e\u108b\u108c\u108d\u108e])\u1037\", \"to\": \"$1$2\u1095\" }, { \"from\": \"\u1095\u1039\", \"to\": \"\u1094\u1039\" }, { \"from\": \"([\u103a\u103b])([\u1000-\u1021])([\u1036\u102d\u102e\u108b\u108c\u108d\u108e]?)\u102f\", \"to\": \"$1$2$3\u1033\" }, { \"from\": \"([\u103a\u103b])([\u1000-\u1021])([\u1036\u102d\u102e\u108b\u108c\u108d\u108e]?)\u1030\", \"to\": \"$1$2$3\u1034\" }, { \"from\": \"\u103a\u102f\", \"to\": \"\u103a\u1033\" }, { \"from\": \"\u103a([\u1036\u102d\u102e\u108b\u108c\u108d\u108e])\u102f\", \"to\": \"\u103a$1\u1033\" }, { \"from\": \"([\u103a\u103b])([\u1000-\u1021])\u1030\", \"to\": \"$1$2\u1034\" }, { \"from\": \"\u103a\u1030\", \"to\": \"\u103a\u1034\" }, { \"from\": \"\u103a([\u1036\u102d\u102e\u108b\u108c\u108d\u108e])\u1030\", \"to\": \"\u103a$1\u1034\" }, { \"from\": \"\u103d\u1030\", \"to\": \"\u1089\" }, { \"from\": \"\u103b([\u1000\u1003\u1006\u100f\u1010\u1011\u1018\u101a\u101c\u101a\u101e\u101f])\", \"to\": \"\u107e$1\" }, { \"from\": \"\u107e([\u1000\u1003\u1006\u100f\u1010\u1011\u1018\u101a\u101c\u101a\u101e\u101f])([\u103c\u108a])([\u1032\u1036\u102d\u102e\u108b\u108c\u108d\u108e])\", \"to\": \"\u1084$1$2$3\" }, { \"from\": \"\u107e([\u1000\u1003\u1006\u100f\u1010\u1011\u1018\u101a\u101c\u101a\u101e\u101f])([\u103c\u108a])\", \"to\": \"\u1082$1$2\" }, { \"from\": \"\u107e([\u1000\u1003\u1006\u100f\u1010\u1011\u1018\u101a\u101c\u101a\u101e\u101f])([\u1033\u1034]?)([\u1032\u1036\u102d\u102e\u108b\u108c\u108d\u108e])\", \"to\": \"\u1080$1$2$3\" }, { \"from\": \"\u103b([\u1000-\u1021])([\u103c\u108a])([\u1032\u1036\u102d\u102e\u108b\u108c\u108d\u108e])\", \"to\": \"\u1083$1$2$3\" }, { \"from\": \"\u103b([\u1000-\u1021])([\u103c\u108a])\", \"to\": \"\u1081$1$2\" }, { \"from\": \"\u103b([\u1000-\u1021])([\u1033\u1034]?)([\u1032\u1036\u102d\u102e\u108b\u108c\u108d\u108e])\", \"to\": \"\u107f$1$2$3\" }, { \"from\": \"\u103a\u103d\", \"to\": \"\u103d\u103a\" }, { \"from\": \"\u103a([\u103c\u108a])\", \"to\": \"$1\u107d\" }, { \"from\": \"([\u1033\u1034])\u1094\", \"to\": \"$1\u1095\" }]";

        return replace_with_rule(rule,input);
    }

    public static String zg12uni51(String input) {


        String rule = "[ { \"from\": \"(\u103d|\u1087)\", \"to\": \"\u103e\" }, { \"from\": \"\u103c\", \"to\": \"\u103d\" }, { \"from\": \"(\u103b|\u107e|\u107f|\u1080|\u1081|\u1082|\u1083|\u1084)\", \"to\": \"\u103c\" }, { \"from\": \"(\u103a|\u107d)\", \"to\": \"\u103b\" }, { \"from\": \"\u1039\", \"to\": \"\u103a\" }, { \"from\": \"\u106a\", \"to\": \"\u1009\" }, { \"from\": \"\u106b\", \"to\": \"\u100a\" }, { \"from\": \"\u106c\", \"to\": \"\u1039\u100b\" }, { \"from\": \"\u106d\", \"to\": \"\u1039\u100c\" }, { \"from\": \"\u106e\", \"to\": \"\u100d\u1039\u100d\" }, { \"from\": \"\u106f\", \"to\": \"\u100d\u1039\u100e\" }, { \"from\": \"\u1070\", \"to\": \"\u1039\u100f\" }, { \"from\": \"(\u1071|\u1072)\", \"to\": \"\u1039\u1010\" }, { \"from\": \"\u1060\", \"to\": \"\u1039\u1000\" }, { \"from\": \"\u1061\", \"to\": \"\u1039\u1001\" }, { \"from\": \"\u1062\", \"to\": \"\u1039\u1002\" }, { \"from\": \"\u1063\", \"to\": \"\u1039\u1003\" }, { \"from\": \"\u1065\", \"to\": \"\u1039\u1005\" }, { \"from\": \"\u1068\", \"to\": \"\u1039\u1007\" }, { \"from\": \"\u1069\", \"to\": \"\u1039\u1008\" }, { \"from\": \"/(\u1073|\u1074)/g\", \"to\": \"\u1039\u1011\" }, { \"from\": \"\u1075\", \"to\": \"\u1039\u1012\" }, { \"from\": \"\u1076\", \"to\": \"\u1039\u1013\" }, { \"from\": \"\u1077\", \"to\": \"\u1039\u1014\" }, { \"from\": \"\u1078\", \"to\": \"\u1039\u1015\" }, { \"from\": \"\u1079\", \"to\": \"\u1039\u1016\" }, { \"from\": \"\u107a\", \"to\": \"\u1039\u1017\" }, { \"from\": \"\u107c\", \"to\": \"\u1039\u1019\" }, { \"from\": \"\u1085\", \"to\": \"\u1039\u101c\" }, { \"from\": \"\u1033\", \"to\": \"\u102f\" }, { \"from\": \"\u1034\", \"to\": \"\u1030\" }, { \"from\": \"\u103f\", \"to\": \"\u1030\" }, { \"from\": \"\u1086\", \"to\": \"\u103f\" }, { \"from\": \"\u1088\", \"to\": \"\u103e\u102f\" }, { \"from\": \"\u1089\", \"to\": \"\u103e\u1030\" }, { \"from\": \"\u108a\", \"to\": \"\u103d\u103e\" }, { \"from\": \"([\u1000-\u1021])\u1064\", \"to\": \"\u1004\u103a\u1039$1\" }, { \"from\": \"([\u1000-\u1021])\u108b\", \"to\": \"\u1004\u103a\u1039$1\u102d\" }, { \"from\": \"([\u1000-\u1021])\u108c\", \"to\": \"\u1004\u103a\u1039$1\u102e\" }, { \"from\": \"([\u1000-\u1021])\u108d\", \"to\": \"\u1004\u103a\u1039$1\u1036\" }, { \"from\": \"\u108e\", \"to\": \"\u102d\u1036\" }, { \"from\": \"\u108f\", \"to\": \"\u1014\" }, { \"from\": \"\u1090\", \"to\": \"\u101b\" }, { \"from\": \"\u1091\", \"to\": \"\u100f\u1039\u1091\" }, { \"from\": \"\u1019\u102c(\u107b|\u1093)\", \"to\": \"\u1019\u1039\u1018\u102c\" }, { \"from\": \"(\u107b|\u1093)\", \"to\": \"\u103a\u1018\" }, { \"from\": \"(\u1094|\u1095)\", \"to\": \"\u1037\" }, { \"from\": \"\u1096\", \"to\": \"\u1039\u1010\u103d\" }, { \"from\": \"\u1097\", \"to\": \"\u100b\u1039\u100b\" }, { \"from\": \"\u103c([\u1000-\u1021])([\u1000-\u1021])?\", \"to\": \"$1\u103c$2\" }, { \"from\": \"([\u1000-\u1021])\u103c\u103a\", \"to\": \"\u103c$1\u103a\" }, { \"from\": \"\u1031([\u1000-\u1021])(\u103e)?(\u103b)?\", \"to\": \"$1$2$3\u1031\" }, { \"from\": \"([\u1000-\u1021])\u1031([\u103b\u103c\u103d\u103e]+)\", \"to\": \"$1$2\u1031\" }, { \"from\": \"\u1032\u103d\", \"to\": \"\u103d\u1032\" }, { \"from\": \"\u103d\u103b\", \"to\": \"\u103b\u103d\" }, { \"from\": \"\u103a\u1037\", \"to\": \"\u1037\u103a\" }, { \"from\": \"\u102f(\u102d|\u102e|\u1036|\u1037)\u102f\", \"to\": \"\u102f$1\" }, { \"from\": \"\u102f\u102f\", \"to\": \"\u102f\" }, { \"from\": \"(\u102f|\u1030)(\u102d|\u102e)\", \"to\": \"$2$1\" }, { \"from\": \"(\u103e)(\u103b|\u1037)\", \"to\": \"$2$1\" }, { \"from\": \"\u1025(\u103a|\u102c)\", \"to\": \"\u1009$1\" }, { \"from\": \"\u1025\u102e\", \"to\": \"\u1026\" }, { \"from\": \"\u1005\u103b\", \"to\": \"\u1008\" }, { \"from\": \"\u1036(\u102f|\u1030)\", \"to\": \"$1\u1036\" }, { \"from\": \"\u1031\u1037\u103e\", \"to\": \"\u103e\u1031\u1037\" }, { \"from\": \"\u1031\u103e\u102c\", \"to\": \"\u103e\u1031\u102c\" }, { \"from\": \"\u105a\", \"to\": \"\u102b\u103a\" }, { \"from\": \"\u1031\u103b\u103e\", \"to\": \"\u103b\u103e\u1031\" }, { \"from\": \"(\u102d|\u102e)(\u103d|\u103e)\", \"to\": \"$2$1\" }, { \"from\": \"\u102c\u1039([\u1000-\u1021])\", \"to\": \"\u1039$1\u102c\" }, { \"from\": \"\u103c\u1004\u103a\u1039([\u1000-\u1021])\", \"to\": \"\u1004\u103a\u1039$1\u103c\" }, { \"from\": \"\u1039\u103c\u103a\u1039([\u1000-\u1021])\", \"to\": \"\u103a\u1039$1\u103c\" }, { \"from\": \"\u103c\u1039([\u1000-\u1021])\", \"to\": \"\u1039$1\u103c\" }, { \"from\": \"\u1036\u1039([\u1000-\u1021])\", \"to\": \"\u1039$1\u1036\" }, { \"from\": \"\u1092\", \"to\": \"\u100b\u1039\u100c\" }, { \"from\": \"\u104e\", \"to\": \"\u104e\u1004\u103a\u1038\" }, { \"from\": \"\u1040(\u102b|\u102c|\u1036)\", \"to\": \"\u101d$1\" }, { \"from\": \"\u1025\u1039\", \"to\": \"\u1009\u1039\" }, { \"from\": \"([\u1000-\u1021])\u103c\u1031\u103d\", \"to\": \"$1\u103c\u103d\u1031\" }, { \"from\": \"([\u1000-\u1021])\u103d\u1031\u103b\", \"to\": \"$1\u103b\u103d\u1031\" }]";

        return replace_with_rule(rule,input);

    }



    public static String replace_with_rule(String rule,String output) {

        try {
            JSONArray rule_array = new JSONArray(rule);
            int max_loop = rule_array.length();

            //because of JDK 7 bugs in Android
            output = output.replace("null","\uFFFF\uFFFF");

            for(int i = 0 ; i < max_loop; i ++) {

                JSONObject obj = rule_array.getJSONObject(i);
                String from = obj.getString("from");
                String to = obj.getString("to");

                output = output.replaceAll(from,to);
                output = output.replace("null","");

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        output = output.replace("\uFFFF\uFFFF","null");
        return output;

    }

//    public static String uni512zg1(String input_text) {
//	String output_text = input_text;
//	output_text = output_text.replaceAll("\\u104e\\u1004\\u103a\\u1038", "\u104e");
//	output_text = output_text.replaceAll("\\u102b\\u103a", "\u105a");
//	output_text = output_text.replaceAll("\\u102d\\u1036", "\u108e");
//	output_text = output_text.replaceAll("\\u103f", "\u1086");
//	output_text = output_text.replaceAll("(?<=\\u102f)\\u1037", "\u1094");
//	output_text = output_text.replaceAll("(?<=\\u102f\\u1036)\\u1037", "\u1094");
//	output_text = output_text.replaceAll("(?<=\\u1030)\\u1037", "\u1094");
//	output_text = output_text.replaceAll("(?<=\\u1030\\u1036)\\u1037", "\u1094");
//	output_text = output_text.replaceAll("(?<=\\u1014)\\u1037", "\u1094");
//	output_text = output_text.replaceAll("(?<=\\u1014[\\u103a\\u1032])\\u1037", "\u1094");
//	output_text = output_text.replaceAll("(?<=\\u103b)\\u1037", "\u1095");
//	output_text = output_text.replaceAll("(?<=\\u103b[\\u1032\\u1036])\\u1037", "\u1095");
//	output_text = output_text.replaceAll("(?<=\\u103d)\\u1037", "\u1095");
//	output_text = output_text.replaceAll("(?<=\\u103d[\\u1032])\\u1037", "\u1095");
//	output_text = output_text.replaceAll("(?<=[\\u103b\\u103c\\u103d])\\u102f", "\u1033");
//	output_text = output_text.replaceAll("(?<=[\\u103b\\u103c\\u103d][\\u102d\\u1036])\\u102f", "\u1033");
//	output_text = output_text.replaceAll("(?<=(\\u1039[\\u1000-\\u1021]))\\u102f", "\u1033");
//	output_text = output_text.replaceAll("(?<=(\\u1039[\\u1000-\\u1021])[\\u102d\\u1036])\\u102f", "\u1033");
//	output_text = output_text.replaceAll("(?<=[\\u100a\\u100c\\u1020\\u1025\\u1029])\\u102f", "\u1033");
//	output_text = output_text.replaceAll("(?<=[\\u100a\\u100c\\u1020\\u1025\\u1029][\\u102d\\u1036])\\u102f", "\u1033");
//	output_text = output_text.replaceAll("(?<=[\\u103b\\u103c])\\u1030", "\u1034");
//	output_text = output_text.replaceAll("(?<=[\\u103b\\u103c][\\u103d])\\u1030", "\u1034");
//	output_text = output_text.replaceAll("(?<=[\\u103b\\u103c][\\u103e])\\u1030", "\u1034");
//	output_text = output_text.replaceAll("(?<=[\\u103b\\u103c][\\u102d\\u1036])\\u1030", "\u1034");
//	output_text = output_text.replaceAll("(?<=[\\u103b\\u103c][\\u103d][\\u103e])\\u1030", "\u1034");
//	output_text = output_text.replaceAll("(?<=[\\u103b\\u103c][\\u103d][\\u102d\\u1036])\\u1030", "\u1034");
//	output_text = output_text.replaceAll("(?<=[\\u103b\\u103c][\\u103e][\\u102d\\u1036])\\u1030", "\u1034");
//	output_text = output_text.replaceAll("(?<=[\\u103b\\u103c][\\u103d][\\u103e][\\u102d\\u1036])\\u1030", "\u1034");
//	output_text = output_text.replaceAll("(?<=(\\u1039[\\u1000-\\u1021]))\\u1030", "\u1034");
//	output_text = output_text.replaceAll("(?<=(\\u1039[\\u1000-\\u1021])[\\u102d\\u1036])\\u1030", "\u1034");
//	output_text = output_text.replaceAll("(?<=[\\u100a\\u100c\\u1020\\u1025\\u1029])\\u1030", "\u1034");
//	output_text = output_text.replaceAll("(?<=[\\u100a\\u100c\\u1020\\u1025\\u1029][\\u102d\\u1036])\\u1030", "\u1034");
//	output_text = output_text.replaceAll("(?<=\\u103c)\\u103e", "\u1087");
//	output_text = output_text.replaceAll("\\u1009(?=[\\u103a])", "\u1025");
//	output_text = output_text.replaceAll("\\u1009(?=\\u1039[\\u1000-\\u1021])", "\u1025");
//	output_text = output_text.replaceAll("([\\u1000-\\u1021\\u1029])((?:\\u1039[\\u1000-\\u1021])?)((?:[\\u103b-\\u103e\\u1087]*)?)\\u1031", "\u1031$1$2$3");
//	output_text = output_text.replaceAll("([\\u1000-\\u1021\\u1029])((?:\\u1039[\\u1000-\\u1021\\u1000-\\u1021])?)(\\u103c)", "$3$1$2");
//	output_text = output_text.replaceAll("\\u1004\\u103a\\u1039", "\u1064");
//	output_text = output_text.replaceAll("(\\u1064)((?:\\u1031)?)((?:\\u103c)?)([\\u1000-\\u1021])\\u102d", "$2$3$4\u108b");
//	output_text = output_text.replaceAll("(\\u1064)((?:\\u1031)?)((?:\\u103c)?)([\\u1000-\\u1021])\\u102e", "$2$3$4\u108c");
//	output_text = output_text.replaceAll("(\\u1064)((?:\\u1031)?)((?:\\u103c)?)([\\u1000-\\u1021])\\u1036", "$2$3$4\u108d");
//	output_text = output_text.replaceAll("(\\u1064)((?:\\u1031)?)((?:\\u103c)?)([\\u1000-\\u1021])", "$2$3$4\u1064");
//	output_text = output_text.replaceAll("\\u100a(?=[\\u1039\\u102f\\u1030])", "\u106b");
//	output_text = output_text.replaceAll("\\u100a", "\u100a");
//	output_text = output_text.replaceAll("\\u101b(?=[\\u102f\\u1030])", "\u1090");
//	output_text = output_text.replaceAll("\\u101b", "\u101b");
//	output_text = output_text.replaceAll("\\u1014(?=[\\u1039\\u103d\\u103e\\u102f\\u1030])", "\u108f");
//	output_text = output_text.replaceAll("\\u1014", "\u1014");
//	output_text = output_text.replaceAll("\\u1039\\u1000", "\u1060");
//	output_text = output_text.replaceAll("\\u1039\\u1001", "\u1061");
//	output_text = output_text.replaceAll("\\u1039\\u1002", "\u1062");
//	output_text = output_text.replaceAll("\\u1039\\u1003", "\u1063");
//	output_text = output_text.replaceAll("\\u1039\\u1005", "\u1065");
//	output_text = output_text.replaceAll("\\u1039\\u1006", "\u1066");
//	output_text = output_text.replaceAll("(?<=[\\u1001\\u1002\\u1004\\u1005\\u1007\\u1012\\u1013\\u108f\\u1015\\u1016\\u1017\\u1019\\u101d])\\u1066", "\u1067");
//	output_text = output_text.replaceAll("\\u1039\\u1007", "\u1068");
//	output_text = output_text.replaceAll("\\u1039\\u1008", "\u1069");
//	output_text = output_text.replaceAll("\\u1039\\u100f", "\u1070");
//	output_text = output_text.replaceAll("\\u1039\\u1010", "\u1071");
//	output_text = output_text.replaceAll("(?<=[\\u1001\\u1002\\u1004\\u1005\\u1007\\u1012\\u1013\\u108f\\u1015\\u1016\\u1017\\u1019\\u101d])\\u1071", "\u1072");
//	output_text = output_text.replaceAll("\\u1039\\u1011", "\u1073");
//	output_text = output_text.replaceAll("(?<=[\\u1001\\u1002\\u1004\\u1005\\u1007\\u1012\\u1013\\u108f\\u1015\\u1016\\u1017\\u1019\\u101d])\\u1073", "\u1074");
//	output_text = output_text.replaceAll("\\u1039\\u1012", "\u1075");
//	output_text = output_text.replaceAll("\\u1039\\u1013", "\u1076");
//	output_text = output_text.replaceAll("\\u1039\\u1014", "\u1077");
//	output_text = output_text.replaceAll("\\u1039\\u1015", "\u1078");
//	output_text = output_text.replaceAll("\\u1039\\u1016", "\u1079");
//	output_text = output_text.replaceAll("\\u1039\\u1017", "\u107a");
//	output_text = output_text.replaceAll("\\u1039\\u1018", "\u107b");
//	output_text = output_text.replaceAll("\\u1039\\u1019", "\u107c");
//	output_text = output_text.replaceAll("\\u1039\\u101c", "\u1085");
//	output_text = output_text.replaceAll("\\u100f\\u1039\\u100d", "\u1091");
//	output_text = output_text.replaceAll("\\u100b\\u1039\\u100c", "\u1092");
//	output_text = output_text.replaceAll("\\u1039\\u100c", "\u106d");
//	output_text = output_text.replaceAll("\\u100b\\u1039\\u100b", "\u1097");
//	output_text = output_text.replaceAll("\\u1039\\u100b", "\u106c");
//	output_text = output_text.replaceAll("\\u100e\\u1039\\u100d", "\u106f");
//	output_text = output_text.replaceAll("\\u100d\\u1039\\u100d", "\u106e");
//	output_text = output_text.replaceAll("\\u1009(?=\\u103a)", "\u1025");
//	output_text = output_text.replaceAll("\\u1025(?=[\\u1039\\u102f\\u1030])", "\u106a");
//	output_text = output_text.replaceAll("\\u1025", "\u1025");
//	output_text = output_text.replaceAll("\\u103a", "\u1039");
//	output_text = output_text.replaceAll("\\u103b\\u103d\\u103e", "\u107d\u108a");
//	output_text = output_text.replaceAll("\\u103d\\u103e", "\u108a");
//	output_text = output_text.replaceAll("\\u103b", "\u103a");
//	output_text = output_text.replaceAll("\\u103c", "\u103b");
//	output_text = output_text.replaceAll("\\u103d", "\u103c");
//	output_text = output_text.replaceAll("\\u103e", "\u103d");
//	output_text = output_text.replaceAll("\\u103a(?=[\\u103c\\u103d\\u108a])", "\u107d");
//	output_text = output_text.replaceAll("(?<=\\u100a(?:[\\u102d\\u102e\\u1036\\u108b\\u108c\\u108d\\u108e]))\\u103d", "\u1087");
//	output_text = output_text.replaceAll("(?<=\\u100a)\\u103d", "\u1087");
//	output_text = output_text.replaceAll("\\u103b(?=[\\u1000\\u1003\\u1006\\u100f\\u1010\\u1011\\u1018\\u101a\\u101c\\u101e\\u101f\\u1021])", "\u107e");
//	output_text = output_text.replaceAll("\\u107e([\\u1000-\\u1021\\u108f])(?=[\\u102d\\u102e\\u1036\\u108b\\u108c\\u108d\\u108e])", "\u1080$1");
//	output_text = output_text.replaceAll("\\u107e([\\u1000-\\u1021\\u108f])(?=[\\u103c\\u108a])", "\u1082$1");
//	output_text = output_text.replaceAll("\\u103b([\\u1000-\\u1021\\u108f])(?=[\\u102d\\u102e\\u1036\\u108b\\u108c\\u108d\\u108e])", "\u107f$1");
//	output_text = output_text.replaceAll("\\u103b([\\u1000-\\u1021\\u108f])(?=[\\u103c\\u108a])", "\u1081$1");
//	output_text = output_text.replaceAll("(?<=\\u1014)\\u1037", "\u1094");
//	output_text = output_text.replaceAll("(?<=\\u1014[\\u103a\\u1032])\\u1037", "\u1094");
//	output_text = output_text.replaceAll("(?<=\\u1033)\\u1094", "\u1095");
//	output_text = output_text.replaceAll("(?<=\\u1033[\\u1036])\\u1094", "\u1095");
//	output_text = output_text.replaceAll("(?<=\\u1034)\\u1094", "\u1095");
//	output_text = output_text.replaceAll("(?<=\\u1034[\\u1036])\\u1094", "\u1095");
//	output_text = output_text.replaceAll("(?<=[\\u103c\\u103d\\u108a])\\u1037", "\u1095");
//	output_text = output_text.replaceAll("(?<=[\\u103c\\u103d\\u108a][\\u1032])\\u1037", "\u1095");
//	return output_text;
//    }
//
//    public static String zg12uni51(String input_text) {
//	String output_text = input_text;
//	output_text = output_text.replaceAll("\\u106a", "\u1009");
//	output_text = output_text.replaceAll("\\u1025(?=[\\u1039\\u102c])", "\u1009");
//	output_text = output_text.replaceAll("\\u1025\\u102e", "\u1026");
//	output_text = output_text.replaceAll("\\u106b", "\u100a");
//	output_text = output_text.replaceAll("\\u1090", "\u101b");
//	output_text = output_text.replaceAll("\\u1040", "\u1040");
//	output_text = output_text.replaceAll("\\u108f", "\u1014");
//	output_text = output_text.replaceAll("\\u1012", "\u1012");
//	output_text = output_text.replaceAll("\\u1013", "\u1013");
//	output_text = output_text.replaceAll("[\\u103d\\u1087]", "\u103e");
//	output_text = output_text.replaceAll("\\u103c", "\u103d");
//	output_text = output_text.replaceAll("[\\u103b\\u107e\\u107f\\u1080\\u1081\\u1082\\u1083\\u1084]", "\u103c");
//	output_text = output_text.replaceAll("[\\u103a\\u107d]", "\u103b");
//	output_text = output_text.replaceAll("\\u103d\\u103b", "\u103b\u103d");
//	output_text = output_text.replaceAll("\\u108a", "\u103d\u103d");
//	output_text = output_text.replaceAll("\\u103d\\u103d", "\u103d\u103d");
//	output_text = output_text.replaceAll("((?:\\u1031)?)((?:\\u103c)?)([\\u1000-\\u1021])\\u1064", "\u1064$1$2$3");
//	output_text = output_text.replaceAll("((?:\\u1031)?)((?:\\u103c)?)([\\u1000-\\u1021])\\u108b", "\u1064$1$2$3\u102d");
//	output_text = output_text.replaceAll("((?:\\u1031)?)((?:\\u103c)?)([\\u1000-\\u1021])\\u108c", "\u1064$1$2$3\u102e");
//	output_text = output_text.replaceAll("((?:\\u1031)?)((?:\\u103c)?)([\\u1000-\\u1021])\\u108d", "\u1064$1$2$3\u1036");
//	output_text = output_text.replaceAll("\\u105a", "\u102b\u103a");
//	output_text = output_text.replaceAll("\\u108e", "\u102d\u1036");
//	output_text = output_text.replaceAll("\\u1033", "\u102f");
//	output_text = output_text.replaceAll("\\u1034", "\u1030");
//	output_text = output_text.replaceAll("\\u1088", "\u103d\u102f");
//	output_text = output_text.replaceAll("\\u1089", "\u103d\u1030");
//	output_text = output_text.replaceAll("\\u1039", "\u103a");
//	output_text = output_text.replaceAll("[\\u1094\\u1095]", "\u1037");
//	output_text = output_text.replaceAll("([\\u1000-\\u1021])([\\u102c\\u102d\\u102e\\u1032\\u1036]){1,2}([\\u1060\\u1061\\u1062\\u1063\\u1065\\u1066\\u1067\\u1068\\u1069\\u1070\\u1071\\u1072\\u1073\\u1074\\u1075\\u1076\\u1077\\u1078\\u1079\\u107a\\u107b\\u107c\\u1085])", "$1$3$2");
//	output_text = output_text.replaceAll("\\u1064", "\u1004\u103a\u1039");
//	output_text = output_text.replaceAll("\\u104e", "\u104e\u1004\u103a\u1038");
//	output_text = output_text.replaceAll("\\u1086", "\u103f");
//	output_text = output_text.replaceAll("\\u1060", "\u1039\u1000");
//	output_text = output_text.replaceAll("\\u1061", "\u1039\u1001");
//	output_text = output_text.replaceAll("\\u1062", "\u1039\u1002");
//	output_text = output_text.replaceAll("\\u1063", "\u1039\u1003");
//	output_text = output_text.replaceAll("\\u1065", "\u1039\u1005");
//	output_text = output_text.replaceAll("[\\u1066\\u1067]", "\u1039\u1006");
//	output_text = output_text.replaceAll("\\u1068", "\u1039\u1007");
//	output_text = output_text.replaceAll("\\u1069", "\u1039\u1008");
//	output_text = output_text.replaceAll("\\u106c", "\u1039\u100b");
//	output_text = output_text.replaceAll("\\u1070", "\u1039\u100f");
//	output_text = output_text.replaceAll("[\\u1071\\u1072]", "\u1039\u1010");
//	output_text = output_text.replaceAll("[\\u1073\\u1074]", "\u1039\u1011");
//	output_text = output_text.replaceAll("\\u1075", "\u1039\u1012");
//	output_text = output_text.replaceAll("\\u1076", "\u1039\u1013");
//	output_text = output_text.replaceAll("\\u1077", "\u1039\u1014");
//	output_text = output_text.replaceAll("\\u1078", "\u1039\u1015");
//	output_text = output_text.replaceAll("\\u1079", "\u1039\u1016");
//	output_text = output_text.replaceAll("\\u107a", "\u1039\u1017");
//	output_text = output_text.replaceAll("\\u107b", "\u1039\u1018");
//	output_text = output_text.replaceAll("\\u107c", "\u1039\u1019");
//	output_text = output_text.replaceAll("\\u1085", "\u1039\u101c");
//	output_text = output_text.replaceAll("\\u106d", "\u1039\u100c");
//	output_text = output_text.replaceAll("\\u1091", "\u100f\u1039\u100d");
//	output_text = output_text.replaceAll("\\u1092", "\u100b\u1039\u100c");
//	output_text = output_text.replaceAll("\\u1097", "\u100b\u1039\u100b");
//	output_text = output_text.replaceAll("\\u106f", "\u100e\u1039\u100d");
//	output_text = output_text.replaceAll("\\u106e", "\u100d\u1039\u100d");
//	output_text = output_text.replaceAll("(\\u103c)([\\u1000-\\u1021])((?:\\u1039[\\u1000-\\u1021])?)", "$2$3$1");
//	output_text = output_text.replaceAll("(\\u103d)(\\u103d)([\\u103b\\u103c])", "$3$2$1");
//	output_text = output_text.replaceAll("(\\u103d)([\\u103b\\u103c])", "$2$1");
//	output_text = output_text.replaceAll("(\\u103d)([\\u103b\\u103c])", "$2$1");
//	//output_text = output_text.replaceAll("(?<=([\\u1000-\\u101c\\u101e-\\u102a\\u102c\\u102e-\\u103d\\u104c-\\u109f]))(\\u1040)(?=\\s)?", "\u101d");
//	//output_text = output_text.replaceAll("(?<=(\\u101d))(\\u1040)(?=\\s)?", "\u101d");
//	output_text = output_text.replaceAll("(?<=([\\u1000-\\u101c\\u101e-\\u102a\\u102c\\u102e-\\u103d\\u104c-\\u109f\\s]))(\\u1047)", "\u101b");
//	output_text = output_text.replaceAll("(\\u1047)(?=[\\u1000-\\u101c\\u101e-\\u102a\\u102c\\u102e-\\u103d\\u104c-\\u109f\\s])", "\u101b");
//	output_text = output_text.replaceAll("((?:\\u1031)?)([\\u1000-\\u1021])((?:\\u1039[\\u1000-\\u1021])?)((?:[\\u102d\\u102e\\u1032])?)([\\u1036\\u1037\\u1038]{0,2})([\\u103b-\\u103d]{0,3})((?:[\\u102f\\u1030])?)([\\u1036\\u1037\\u1038]{0,2})((?:[\\u102d\\u102e\\u1032])?)", "$2$3$6$1$4$9$7$5$8");
//	output_text = output_text.replaceAll("\\u1036\\u102f", "\u102f\u1036");
//	output_text = output_text.replaceAll("(\\u103a)(\\u1037)", "$2$1");
//	return output_text;
//    }

}
