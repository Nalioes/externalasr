package com.dasr.devasrtwo;

import java.io.File;

public class VoiceObj {
    private File file;
    private String name;

    public VoiceObj(File file, String name) {
        this.file = file;
        this.name = name;
    }

    public VoiceObj(String name) {
        this.name = name;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
