package com.dasr.devasrtwo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dasr.devasrtwo.recorder.AbstractRecorder;
import com.dasr.devasrtwo.recorder.AudioChunk;
import com.dasr.devasrtwo.recorder.AudioRecordConfig;
import com.dasr.devasrtwo.recorder.OmRecorder;
import com.dasr.devasrtwo.recorder.PullTransport;
import com.dasr.devasrtwo.recorder.PullableSource;
import com.dasr.devasrtwo.recorder.Recorder;
import com.dasr.devasrtwo.recorder.WriteAction;
import com.google.common.io.Files;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AudioView.OnRecordListener {
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    final int[] progress = {0};
    public boolean mIsRecording = false;
    Recorder recorder;
    Runnable mCallBacks;
    int apiCount = 0;
    private int RECORD_AUDIO_REQUEST_CODE = 123;
    private AudioView recordView;
    private Handler mHandler;
    private String mFileName;
    private CountDownTimer timer;
    private int maxInterval = 10000;
    private ProgressBar clipProgress;
    private boolean isBeginAccessToApi = false;
    private boolean isOverClip = false;
    private boolean isUnicode = false;
    private String content;
    private TextView title;
    private TextView msThree;
    RecyclerView recyclerView;
    FileAdapter adapter;
    private String fileName;
    private int fileNameCount = 0;

    private @SuppressLint("StaticFieldLeak")
    RequestASRAsync async;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        title = findViewById(R.id.txtTitle);
        recordView = findViewById(R.id.recordView);
        clipProgress = findViewById(R.id.clip_progress);
        recyclerView = findViewById(R.id.recycler);

        msThree = findViewById(R.id.threems);
        clipProgress.setProgress(0);
        mHandler = new Handler(Looper.getMainLooper());

        if (FontDetect.isUnicode()) {
            isUnicode = true;
        }
        if (isUnicode) {
            content = Converter.zg12uni51("ဖိထား ၍ စကားေျပာပါ");
        } else {
            content = "ဖိထား ၍ စကားေျပာပါ";
        }

        title.setText(content);

        if (getRecordPermissionGrant()) {
            initRecording();
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(true);
        adapter = new FileAdapter(this);
        recyclerView.setAdapter(adapter);
        collectFiles();
    }

    private void initRecording() {
        if (validateMicAvailability()) {
            recordView.setOnRecordListener(this);
        } else {
            if (recorder != null) {
                try {
                    recorder.stopRecording();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            recordView.setOnRecordListener(this);
        }
        setupRecorder();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRecordStart() {
        try {
            mIsRecording = true;
            mHandler.post(mCallBacks);
        } catch (Exception e) {
            Toast.makeText(MainActivity.this, "MediaRecorder prepare failed!", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    public void resetAudio() {
        mIsRecording = false;
        progress[0] = 0;
        if (recorder != null) {
            try {
                recorder.stopRecording();
                recorder = null;
                if (timer != null) {
                    timer.cancel();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRecordFinish() {
        mIsRecording = false;
        progress[0] = 0;
        if (recorder != null) {
            try {
                recorder.stopRecording();
                recorder = null;
                if (timer != null) {
                    timer.cancel();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void setupRecorder() {
        timer = new CountDownTimer(maxInterval, 1000) {
            @Override
            public void onTick(long l) {
                clipProgress.setProgress(++progress[0]);

            }

            @Override
            public void onFinish() {
                if (recorder != null) {
                    mIsRecording = false;
                    isOverClip = true;
                    progress[0] = 0;
                    Toast.makeText(MainActivity.this, "Recording voice clip must be within 10 seconds.", Toast.LENGTH_SHORT).show();
                    try {
                        if (!mIsRecording) {
                            clipProgress.setProgress(0);
                            if (isUnicode) {
                                content = Converter.zg12uni51("ဖိထား ၍ စကားေျပာပါ");
                            } else {
                                content = "ဖိထား ၍ စကားေျပာပါ";
                            }
                            title.setText(content);
                            recordView.animateRadius(0);
                        }
                        collectFiles();
                        recorder.stopRecording();
                        recorder = null;
                        if (timer != null) {
                            timer.cancel();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        mCallBacks = new Runnable() {
            @Override
            public void run() {
                if (recorder == null) {
                    recorder = initRecorder();
                }
                timer.start();
                recorder.startRecording();
                isOverClip = false;
            }
        };
    }

    public boolean getRecordPermissionGrant() {
        boolean isGrant = false;
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.RECORD_AUDIO)
                    == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                isGrant = true;
            } else {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    View view = getCurrentFocus();
                    if (view == null)
                        view = new View(MainActivity.this);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, RECORD_AUDIO_REQUEST_CODE);
                isGrant = false;
            }
        } else {
            isGrant = true;
        }
        return isGrant;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        if (requestCode == RECORD_AUDIO_REQUEST_CODE) {
            if (grantResults.length == 3 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                initRecording();
            }
        }
    }

    private boolean validateMicAvailability() {
        Boolean available = true;
        AudioRecord recorder =
                new AudioRecord(MediaRecorder.AudioSource.DEFAULT, 44100,
                        AudioFormat.CHANNEL_IN_MONO,
                        AudioFormat.ENCODING_DEFAULT, 44100);
        try {
            if (recorder.getRecordingState() != AudioRecord.RECORDSTATE_STOPPED) {
                available = false;
            }

            recorder.startRecording();
            if (recorder.getRecordingState() != AudioRecord.RECORDSTATE_RECORDING) {
                recorder.stop();
                available = false;
            }
            recorder.stop();
        } finally {
            recorder.release();
            recorder = null;
        }

        return available;
    }

    public boolean isFilePresent(String fileName) {
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "/voice/" + fileName);
        return file.exists();
    }

    public File getFile(Context context, String fileName) {
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "/voice/" + fileName);
        if (file.exists()) {
            return file;
        } else {
            return null;
        }
    }

    public void deleteExistingFile(String fileName) {
        File file = new File(getFilesDir().getAbsolutePath() + "/voice/" + fileName);
        if (file != null) {
            if (file.exists()) {
                boolean deletedFile = file.delete();
            }
        }
    }

    private PullableSource mic() {
        return new PullableSource.NoiseSuppressor(
                new PullableSource.Default(
                        new AudioRecordConfig.Default(
                                MediaRecorder.AudioSource.DEFAULT, AudioFormat.ENCODING_PCM_16BIT,
                                AudioFormat.CHANNEL_IN_MONO, 16000
                        )
                )
        );
    }

    @NonNull
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    private File file() {
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "/voice");
        if (!file.exists()) {
            file.mkdir();
        }
        if (!TextUtils.isEmpty(fileName)) {
            fileNameCount = Integer.valueOf(fileName);
        }
        fileName = fileNameCount + 1 + "";
        mFileName = fileName;

        return new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "/voice/" + mFileName + ".wav");
    }


    @RequiresApi(Build.VERSION_CODES.KITKAT)
    public String cryptoHashByID(String raw) throws NoSuchAlgorithmException {
//        MessageDigest md = MessageDigest.getInstance("SHA-256");
//        md.update(raw.getBytes(StandardCharsets.UTF_16));
//        byte[] digest = md.digest();
//
//        return String.format("%064x", new BigInteger(1, digest));
        return raw;
    }

    public Recorder initRecorder() {
        recorder = OmRecorder.wav(
                new PullTransport.Noise(mic(),
                        new PullTransport.OnAudioChunkPulledListener() {
                            @Override
                            public void onAudioChunkPulled(final AudioChunk audioChunk) {
                                mHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (!isBeginAccessToApi) {
                                            if (isUnicode) {
                                                content = Converter.zg12uni51("နားေထာင္ေနပါသည္...");
                                            } else {
                                                content = "နားေထာင္ေနပါသည္...";
                                            }
                                            title.setText(content);
                                        }
                                        if (!mIsRecording) {
                                            clipProgress.setProgress(0);
                                            if (!isBeginAccessToApi) {
                                                if (isUnicode) {
                                                    content = Converter.zg12uni51("ဖိထား ၍ စကားေျပာပါ");
                                                } else {
                                                    content = "ဖိထား ၍ စကားေျပာပါ";
                                                }
                                                title.setText(content);
                                            }
                                            recordView.animateRadius(0);
                                        }

                                        if (mIsRecording) {
                                            mHandler.postDelayed(this, 50);
                                        }
                                    }
                                });
                            }
                        },
                        new WriteAction.Default(),
                        new Recorder.OnSilenceListener() {
                            @Override
                            public void onSilence(long silenceTime) {
                            }
                        }, 500
                ), file(), new AbstractRecorder.OnFinishOutputStream() {
                    @SuppressLint("StaticFieldLeak")
                    @Override
                    public void oNFinish(boolean isFinish) {
                        if (isFinish && getFile(MainActivity.this, mFileName + ".wav") != null && !isOverClip) {
                            if (isUnicode) {
                                content = Converter.zg12uni51("ဖိထား ၍ စကားေျပာပါ");
                            } else {
                                content = "ဖိထား ၍ စကားေျပာပါ";
                            }
                            title.setText(content);
                            collectFiles();
                        }
                    }
                }
        );
        return recorder;
    }


    private void collectFiles() {
        List<VoiceObj> voiceObjs = new ArrayList<>();
        File directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "/voice");
        if (!directory.exists())directory.mkdir();
        List<File> files = Arrays.asList(directory.listFiles());
        Collections.sort(files, new FileComparator());
        for (int i = 0; i < files.size(); i++) {
            if (files.get(i) != null) {
                if (!TextUtils.isEmpty(files.get(i).getName())) {
                    voiceObjs.add(new VoiceObj(files.get(i).getName()));
                }
            }
        }
        if (voiceObjs.size() > 0) {
            fileName = Files.getNameWithoutExtension(voiceObjs.get(voiceObjs.size() - 1).getName());
            Collections.reverse(voiceObjs);
            adapter.addItem(voiceObjs);
            msThree.setText("( " + voiceObjs.size() + "  )");

        } else {
            msThree.setText("( " + 0 + "  )");
        }
    }

    private RequestASRAsync getAsync() {

        @SuppressLint("StaticFieldLeak") RequestASRAsync asrAsync = new RequestASRAsync(MainActivity.this, getFile(MainActivity.this, mFileName + ".wav")) {
            @Override
            protected void onPreExecute() {
                isBeginAccessToApi = true;
                if (isUnicode) {
                    content = Converter.zg12uni51("လုပ္္ေဆာင္ေနပါသည္။ ေခတၱေစာင့္ပါ။");
                } else {
                    content = "လုပ္္ေဆာင္ေနပါသည္။ ေခတၱေစာင့္ပါ။";
                }
                title.setText(content);
                recordView.setEnabled(false);

            }

            @Override
            protected void onPostExecute(List<String> s) {
                try {
                    JSONObject object = new JSONObject(s.get(0));
                    requestAsrModel(object.getString("value"), s);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(MainActivity.this, s.get(0), Toast.LENGTH_SHORT).show();
                }
            }

        };

        return asrAsync;
    }

    private void requestAsrModel(String response, List<String> raw) {
        if (apiCount == 0) {

            ++apiCount;
            if (async.getStatus() == AsyncTask.Status.RUNNING) {
                async.cancel(true);
                async = null;
                async = getAsync();
                async.execute("https://elb.baganasr.com:8443/node/api/v1/get-text-dev-2");
            }
        } else if (apiCount == 1) {
            ++apiCount;
            if (async.getStatus() == AsyncTask.Status.RUNNING) {
                async.cancel(true);
                async = null;
                async = getAsync();
                async.execute("https://elb.baganasr.com:8443/node/api/v1/get-text-dev-3");
            }
        } else {
            isBeginAccessToApi = false;
            if (isUnicode) {
                content = Converter.zg12uni51("ဖိထား ၍ စကားေျပာပါ");
            } else {
                content = "ဖိထား ၍ စကားေျပာပါ";
            }
            title.setText(content);
            recordView.setEnabled(true);
            msThree.setText("( " + raw.get(1) + " ms )");
            apiCount = 0;
        }


    }

    public static class RequestASRAsync extends AsyncTask<String, Void, List<String>> {
        File mFile;
        Context context;
        List<String> result = new ArrayList<>();

        public RequestASRAsync(Context context, File file) {
            mFile = file;
            this.context = context;
        }

        @Override
        protected List<String> doInBackground(String... strings) {
            try {
                HTTPPost post = new HTTPPost();
                result = post.processAsrAsynchronous(context, mFile, strings[0], true, null);
                return result;
            } catch (Exception e) {
                e.printStackTrace();
                result.add("Connection timeout!!!Please try again...");
                return result;
            }
        }
    }

    public class FileComparator implements Comparator<File> {
        @RequiresApi(Build.VERSION_CODES.KITKAT_WATCH)
        public int compare(File x, File y) {
            return Long.compare(x.lastModified(), y.lastModified());
        }
    }
}
