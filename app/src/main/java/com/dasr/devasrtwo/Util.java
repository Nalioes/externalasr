package com.dasr.devasrtwo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class Util {
    public static boolean isBaganKB(Context context) {
        String imeName = Secure.getString(
                context.getContentResolver(),
                Secure.DEFAULT_INPUT_METHOD);
        return imeName.startsWith("com.bit.androsmart.kbinapp");

    }


    public static void optimize(final Context context) {
        new AsyncTask<String, String, String>() {
            @Override
            protected String doInBackground(String... params) {
                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
                sp.edit().putBoolean("handle_mm_delete", true)
                        .putBoolean("quick_fix", true)
                        .commit();
                return null;
            }
        }.execute("");

    }

    public static void appInstallFromPlayStore(Context context, String packageName) {

        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (android.content.ActivityNotFoundException anfe) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }


    public static boolean isOnline(Context ctx) {

        ConnectivityManager cm = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();

    }

    public static boolean isWifiConnecting(Context ctx) {
        final ConnectivityManager connMgr = (ConnectivityManager)
                ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return wifi.isConnectedOrConnecting();
    }

    public static boolean isMobileConnecting(Context ctx) {
        final ConnectivityManager connMgr = (ConnectivityManager)
                ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        return mobile.isConnectedOrConnecting();
    }

    public static void popupAlert(final Activity context, String message,
                                  final Class<?> cls) {
        AlertDialog.Builder d = new AlertDialog.Builder(context);
        d.setMessage(message);
        d.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent it = new Intent(context.getApplicationContext(), cls);
                it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(it);
            }
        });
        d.show();

    }

    public static String getUuid(Context context) {
        final String s;
        try {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            if (tm.getDeviceId() != null) {
                s = tm.getDeviceId();
                // s ="0000000000000000";
            } else {
                // s ="0000000000000000";
                s = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
            }
            return s;
        } catch (Exception e) {
            return "noudid";
        }
    }


    public static boolean isAppInstalled(Context context, String packageName) {
        try {
            context.getPackageManager().getApplicationInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static String getAndriodID(Context context) {
        return Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
    }

    public static String hashToMD5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest.getInstance(MD5);
            digest.update(s.getBytes());
            byte[] messageDigest = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static boolean checkPremium(String input, Context context) {
        return input.equals(hashToMD5(getAndriodID(context).substring(0, 10)));
    }

    public static boolean checkPremiumOld(String input, Context context) {
        return input.equals(hashToMD5(getAndriodID(context).substring(0, 7)));
    }

    public static JSONObject buildJSON(String[] strings) throws JSONException {
        if ((strings.length % 2) != 0) return null; // odd length
        JSONObject jo = new JSONObject();
        for (int i = 0; i < strings.length; i++) {
            jo.put(strings[i], strings[i + 1]);
            i++;
        }

        return jo;
    }


    public static String getMCC(Context context) {
        int mcc = 0;
        try {
            TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            String networkOperator = manager.getNetworkOperator();
            if (networkOperator != null) {
                mcc = Integer.parseInt(networkOperator.substring(0, 3));

            }

        } catch (Exception e) {
        }
        return String.valueOf(mcc);
    }

    public static String getMNC(Context context) {
        int mnc = 0;
        try {
            TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            String networkOperator = manager.getNetworkOperator();
            if (networkOperator != null) {
                mnc = Integer.parseInt(networkOperator.substring(3));

            }

        } catch (Exception e) {
        }
        return String.valueOf(mnc);
    }

    public static boolean isPackageExisted(Context context, String targetPackage) {
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(targetPackage, PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
        return true;
    }

}
