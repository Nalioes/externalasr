package com.dasr.devasrtwo;

public class TemplateItem {
    private String category;
    private String sub_category;
    private String fun;
    private String command_name;

    public TemplateItem() {
    }

    public TemplateItem(String category, String sub_category, String fun, String command_name) {
        this.category = category;
        this.sub_category = sub_category;
        this.fun = fun;
        this.command_name = command_name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSub_category() {
        return sub_category;
    }

    public void setSub_category(String sub_category) {
        this.sub_category = sub_category;
    }

    public String getFun() {
        return fun;
    }

    public void setFun(String fun) {
        this.fun = fun;
    }

    public String getCommand_name() {
        return command_name;
    }

    public void setCommand_name(String command_name) {
        this.command_name = command_name;
    }
}
