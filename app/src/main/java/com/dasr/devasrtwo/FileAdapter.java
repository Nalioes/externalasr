package com.dasr.devasrtwo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.dasr.devasrtwo.databinding.ItemVoiceBinding;

import java.util.ArrayList;
import java.util.List;

public class FileAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ViewListener listener;
    private List<VoiceObj> items = new ArrayList<>();
    private Context context;

    public FileAdapter(Context context) {
        this.context = context;
    }

    public void setListener(ViewListener listener) {
        this.listener = listener;
    }

    public void addItem(List<VoiceObj> items) {
        this.items = items;
        notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_voice, parent, false).getRoot());
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final VoiceObj obj = items.get(position);
        ((ItemViewHolder) holder).getBinding().txt.setText(obj.getName());



    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ItemVoiceBinding binding;

        public ItemViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            itemView.setOnClickListener(this);
        }

        public ItemVoiceBinding getBinding() {
            return binding;
        }

        @Override
        public void onClick(View view) {
            if (listener != null) {
                listener.received(getAdapterPosition());
            }
        }
    }

    public interface ViewListener {
        void received(int position);
    }

    public interface OnViewClickListener {
        void onClick(int position);
    }
}